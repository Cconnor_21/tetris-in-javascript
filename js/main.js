const COLS = 10;
const ROWS = 20;
const BLOCK_SIZE = 30;

//get the canvas
const canvas = document.getElementById('board');
const ctx = canvas.getContext('2d');

//calculate the size of the canvas from constraints
ctx.canvas.width = COLS * BLOCK_SIZE;
ctx.canvas.height = ROWS * BLOCK_SIZE;

ctx.scale(BLOCK_SIZE, BLOCK_SIZE);
